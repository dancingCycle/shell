#!/bin/sh
#
echo "started..."
#
# special variable $# is the number of arguments
if [ $# -lt 2 ] ; then
    echo 'Call ./<script> <absolute file name> <minimum file size in bytes>'
    exit 1
fi
#
file="$1"
echo "file: ${file}"
#
if [ ! -f $file ]; then
    echo "ERROR: File not found!"
    exit 1
fi
#
minimumSize="$2"
echo "minimumSize: ${minimumSize}"
now=$(date +%Y%m%d%H%M%S)
echo "now: ${now}"
actualSize=$(wc -c <"$file")
echo "actualSize: ${actualSize}"
output=""
#
if [ $actualSize -ge $minimumSize ]; then
    output="$now: File ${file} size is greater than or equal minimum size of $minimumSize Bytes. Actual size: $actualSize Bytes"
    echo $output
else
    output="$now: File ${file} size is less than minimum size of $minimumSize Bytes. Actual size: $actualSize Bytes"
    echo $output
    exit 1
fi
#
echo "done."
