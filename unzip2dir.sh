#!/bin/sh
#
echo "Started..."
#Started...
#
# special variable $# is the number of arguments
if [ $# -lt 3 ] ; then
    echo 'Call ./<script> <abs zip file path without /> <zip file name> <abs unzip dir>'
    exit 1
fi
#
zipFilePath="$1"
echo "zipFilePath: $zipFilePath"
#
zipFile=${zipFilePath}/"$2"
echo "zipFile: $zipFile"
#
unzipDir="$3"
echo "unzipDir: $unzipDir"
#
baseDir=$(dirname $0)
echo "baseDir: $baseDir"
#
# extract gtfs archive
rm -rf $unzipDir
mkdir -p $unzipDir
unzip -qq -d $unzipDir $zipFile
#
echo "Done."
#done.
