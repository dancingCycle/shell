#!/bin/sh
#
echo "started..."
#
# special variable $# is the number of arguments
if [ $# -lt 4 ] ; then
    echo 'Call ./<script> <url> <cert secret> <cert file path & name> <save as abs file path & name with SUFFIX /<file name>.gz>'
    exit 1
fi
#
downloadUrl="$1"
certSecret="$2"
certPath="$3"
#output: ATTENTION: use SUFFIX ".../<file name>.gz"
output="$4"
echo "output: ${output}"
#
curl --request GET --url $downloadUrl --header "Accept-Encoding: gzip, deflate"  --cert-type P12 --cert ${certPath}:"$certSecret" --output $output
#
echo "done."
