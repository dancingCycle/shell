#!/bin/bash
#
# special variable $# is the number of arguments
#
# Start...
#
if [ $# -lt 2 ] ; then
    echo 'Call ./<script> <input file> <output file>'
    exit 1
fi
#
INPUT_FILE="$1"
#echo "INPUT_FILE: $INPUT_FILE"
OUTPUT_FILE="$2"
#echo "OUTPUT_FILE: $OUTPUT_FILE"
#
mlr --csv --fs=';' put '
    $oeffnungszeiten = gssub(gssub(gssub(gssub(gssub(gssub(gssub(gssub($oeffnungszeiten,
        "Tu", "Di"),
        "We", "Mi"),
        "Th", "Do"),
        "Su", "So"),
        "PH", "Feiertag"),
        "SH", "Schulferien"),
        "off", "geschlossen"),
        "OFF", "geschlossen"
    );
' $INPUT_FILE > $OUTPUT_FILE
#
# ...done.
#
