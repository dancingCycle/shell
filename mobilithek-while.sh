#!/bin/sh
#
echo "started..."
#
# special variable $# is the number of arguments
if [ $# -lt 5 ] ; then
    echo 'Call ./<script> <size: absolute file name> <size: minimum file size in bytes> <dload: url> <dload: cert secret> <dload: cert file path & name>'
    exit 1
fi
#
file="$1"
minimumSize="$2"
dloadUrl="$3"
dloadCertKey="$4"
dloadCertFile="$5"
#
retVal=1;
#echo "retVal initial state: ${retVal}"
#
fileRemove=$file
#echo "fileRemove: ${fileRemove}"
fR1=${fileRemove%?}
fR2=${fR1%?}
fR3=${fR2%?}
rm $fR3
#
echo "call script: file-dload-curl-cert-while.sh"
sh ./file-dload-curl-cert-while.sh $file $minimumSize $dloadUrl $dloadCertKey $dloadCertFile
#
gunzip -q $file
#
echo "done."
