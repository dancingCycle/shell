#!/bin/sh
#
# special variable $# is the number of arguments
if [ $# -lt 7 ] ; then
    echo 'Call ./<script> <host> <port> <user> <database> <format> <output path without trailing slash> <output file name without suffix>'
    exit 1
fi
#
hst="$1"
prt="$2"
usr="$3"
dtbs="$4"
frmt="$5"
otpt_pth="$6"
otpt_fl="$7"
otpt="${otpt_pth}/${otpt_fl}"
#
tmstmp=$(date +"%Y-%m-%d_%H-%M-%S")
#
if [ "$frmt" = "plain" ]; then
    pg_dump -h $hst -p $prt -U $usr -d $dtbs | gzip > "${otpt}-${tmstmp}-fPlain.gz"
elif [ "$frmt" = "custom" ]; then
    pg_dump -h $hst -p $prt -U $usr -d $dtbs -F custom | gzip > "${otpt}-${tmstmp}-fCustom.gz"
else	 
    echo "ERROR: Format unknown."
fi

