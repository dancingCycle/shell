#!/bin/sh
#
echo "started..."
#
# special variable $# is the number of arguments
if [ $# -lt 5 ] ; then
    echo 'Call ./<script> <size: absolute file name> <size: minimum file size in bytes> <dload: url> <dload: cert secret> <dload: cert file path & name>'
    exit 1
fi
#
file="$1"
minimumSize="$2"
dloadUrl="$3"
dloadCertKey="$4"
dloadCertFile="$5"
#
retVal=1;
#echo "retVal initial state: ${retVal}"
#
while [ $retVal -ne 0 ]
do
    echo "call script: file-dload-curl-cert.sh"
    sh ./file-dload-curl-cert.sh $dloadUrl $dloadCertKey $dloadCertFile $file
    echo "call script: file-size-without-email.sh"
    #TODO This looks like an asynchronous we are not waiting for, isn't it?
    sh ./file-size-without-email.sh $file $minimumSize
    retVal=$?
    #echo "retVal: ${retVal}"
done
exit $retVal
#
echo "done."
