#!/bin/sh
#
echo "started..."
#
# special variable $# is the number of arguments
if [ $# -lt 6 ] ; then
    echo 'Call ./<script> <size: absolute file name> <size: minimum file size in bytes> <size: csv email recipient list> <dload: work dir> <dload: save as file name> <dload: URL>'
    exit 1
fi
#
file="$1"
minimumSize="$2"
emailRecipientList="$3"
workDir="$4"
fileName="$5"
dloadUrl="$6"
#
retVal=1;
echo "retVal init: ${retVal}"
#
while [ $retVal -ne 0 ]
do
    echo "call file-dload.sh"
    sh ./file-dload.sh $workDir $fileName $dloadUrl
    echo "call file-size.sh"
    sh ./file-size.sh $file $minimumSize $emailRecipientList
    retVal=$?
    echo "retVal: ${retVal}"
done
exit $retVal
#
echo "done."
