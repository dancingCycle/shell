#!/bin/sh
#
echo "started..."
#Started...
#
# special variable $# is the number of arguments
if [ $# -lt 3 ] ; then
    echo 'Call ./<script> <work dir without /> <save as file name> <dload URL>'
    exit 1
fi
#
workDir="$1"
echo "workDir: $workDir"
#
fileName="$2"
echo "fileName: $fileName"
#
dloadUrl="$3"
echo "dloadUrl: $dloadUrl"
#
wgetLogFile=${workDir}/wget-log-${fileName}
echo "wgetLogFile: $wgetLogFile"
#
dloadPath=${workDir}/${fileName}
echo "dloadPath: $dloadPath"
#
rm -f $dloadPath
rm -f ${wgetLogFile}
wget -o $wgetLogFile -O $dloadPath $dloadUrl
#
echo "done."
