#!/bin/sh
#
#echo "started..."
#
# special variable $# is the number of arguments
if [ $# -lt 4 ] ; then
    echo 'Call ./<script> <body> <sender address> <subject> <recipient address list>'
    exit 1
fi
#
bdy="$1"
##echo "bdy: ${bdy}"
#
sndr="$2"
##echo "sndr: ${sndr}"
#
sbjct="$3"
##echo "sbjct: ${sbjct}"
#
rcpnt="$4"
##echo "rcpnt: ${rcpnt}"
#
echo ${bdy} | mail -s "${sbjct}" -r ${sndr} ${rcpnt}
#
#echo "done."
