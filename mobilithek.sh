#!/bin/sh
#
echo "started..."
#
# special variable $# is the number of arguments
if [ $# -lt 4 ] ; then
    echo 'Call ./<script> <save as abs file path & name with SUFFIX /<file name>.gz> <url> <cert secret> <cert file path & name>'
    exit 1
fi
#
file="$1"
dloadUrl="$2"
dloadCertKey="$3"
dloadCertFile="$4"
#
retVal=1;
#echo "retVal initial state: ${retVal}"
#
fileRemove=$file
#echo "fileRemove: ${fileRemove}"
fR1=${fileRemove%?}
fR2=${fR1%?}
fR3=${fR2%?}
rm $fR3
#
echo "call script: file-dload-curl-cert.sh"
sh ./file-dload-curl-cert.sh $dloadUrl $dloadCertKey $dloadCertFile $file
#
gunzip -q $file
#
echo "done."
