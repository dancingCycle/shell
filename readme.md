# shell

shell scripting

## file size

```
sh ./file-size.sh ~/Downloads/....pdf 19779 ...@posteo.de,...@hostsharing.net
```

## file download while loop

```
sh ./file-dload-while.sh ~/Downloads/....pbf 19800967 ...@sposteo.de,...@hostsharing.net ~/Downloads ....pbf http://download.geofabrik.de/europe/germany/bremen-latest.osm.pbf
```

## file-dload-curl-cert

```
sh file-dload-curl-cert.sh https://mobilithek.info:8443/mobilithek/api/v1.0/container/subscription?subscriptionID=... <key> ~/.../certificate.p12 ~/.../foo/gtfs.gz
```

## file download using curl --cert while loop

```
sh ./file-dload-curl-cert-while.sh ~/.../gtfs.gz 49800700 ...@hostsharing.net https://mobilithek.info:8443/mobilithek/api/v1.0/container/subscription?subscriptionID=... <key> ~/.../certificate.p12 ~/.../foo/gtfs.gz
```

## swap OSM openting_hours tag value from english to german abbreviation

```
/osm-opening_hours2de.sh ~/Downloads/poi_system/poi-poi\@SRV-WEB-02-2024-11-25.csv ~/Downloads/poi_system/poi-poi\@SRV-WEB-02-2024-11-25-de.csv
```
