#!/bin/sh
#
echo "started..."
#
# special variable $# is the number of arguments
if [ $# -lt 3 ] ; then
    echo 'Call ./<script> <absolute file name> <minimum file size in bytes> <csv email recipient list>'
    exit 1
fi
#
file="$1"
echo "file: ${file}"
#
if [ ! -f $file ]; then
    echo "ERROR: File not found!"
    exit 1
fi
#
minimumSize="$2"
echo "minimumSize: ${minimumSize}"
emailRecipientList="$3"
#echo "emailRicipientList: ${emailRecipientList}"
now=$(date +%Y%m%d%H%M%S)
echo "now: ${now}"
actualSize=$(wc -c <"$file")
echo "actualSize: ${actualSize}"
output=""
#
if [ $actualSize -ge $minimumSize ]; then
    output="$now: File ${file} size is greater than or equal minimum size of $minimumSize Bytes. Actual size: $actualSize Bytes"
    echo $output
    echo $output | mail -s "File size valid: ${file}" ${emailRecipientList}
else
    output="$now: File ${file} size is less than minimum size of $minimumSize Bytes. Actual size: $actualSize Bytes"
    echo $output
    echo $output | mail -s "File size NOT valid: ${file}" ${emailRecipientList}
    exit 1
fi
#
echo "done."
